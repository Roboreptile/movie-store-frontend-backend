import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ShopComponent } from './shop/shop.component';

import {MenuModule} from 'primeng/menu';
import {TableModule} from 'primeng/table';
import {ButtonModule} from 'primeng/button';
import {InputTextModule} from 'primeng/inputtext';
import {RadioButtonModule} from 'primeng/radiobutton';
import {MenuItem} from 'primeng/api';
import {MegaMenuModule} from 'primeng/megamenu';
import {ConfirmationService, MessageService} from "primeng/api";
import { ConfirmPopupModule } from "primeng/confirmpopup";
import { ToastModule } from "primeng/toast";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import {CalendarModule} from 'primeng/calendar';

@NgModule({
  declarations: [
    AppComponent,
    ShopComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MenuModule,
    TableModule,
    ButtonModule,
    MegaMenuModule,
    InputTextModule,
    RadioButtonModule,
    FormsModule,
    ToastModule,
    ConfirmPopupModule,
    BrowserAnimationsModule,
    CalendarModule,
  ],
  providers: [ConfirmationService,MessageService],
  bootstrap: [AppComponent,ShopComponent]
})
export class AppModule { }
