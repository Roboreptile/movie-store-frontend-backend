export interface Order{
    ID: number,
    MovieID: number;
    Date: string;
    Net: number;
    Discount?: number;
    Gross: number;
}