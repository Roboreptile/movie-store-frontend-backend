import { Component, OnInit } from '@angular/core';
import {MenuItem} from 'primeng/api';
import {MenuModule} from 'primeng/menu';
import {TableModule} from 'primeng/table';
import {ButtonModule} from 'primeng/button';
import {MegaMenuModule} from 'primeng/megamenu';
import {InputTextModule} from 'primeng/inputtext';
import {RadioButtonModule} from 'primeng/radiobutton';
import {CalendarModule} from 'primeng/calendar';

import { Movie } from './movies_type';
import { Order } from './orders_type';
import { ConfirmationService, MessageService} from "primeng/api";

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.css']
})
export class ShopComponent implements OnInit {
  selectedGenre:string = "All";
  devMode:boolean = false;
  movies: Movie[] = [];
  orders: Order[] = [];

  dateFrom:string = "";
  dateTo:string = "";

  constructor(
    private cservice: ConfirmationService,
    private service: MessageService
  ) { }

  ngOnInit(): void {
    this.getAllMovies()
  }

  devModeSwap(): void {
    this.devMode = !this.devMode;
    if(this.devMode == true){
      this.getAllOrders();
    } else {
      this.getAllMovies();
    }
  }
 
  async applyFilters(): Promise<void>{
    const titleF = (document.getElementById("title-filter") as HTMLInputElement).value;
    const rankF = (document.getElementById("rank-filter") as HTMLInputElement).value;
    const genreF = this.selectedGenre;

    await this.getAllMovies();
    let newMovies: Movie[] = [];

    this.movies.forEach((movie) => {
      if(movie.Title.toLowerCase().includes(titleF.toLowerCase()) &&
       ( rankF == "" || movie.Ranking>=Number.parseFloat(rankF)) &&
       ( genreF == "All" || movie.Genre == genreF) &&
       ( this.dateFrom == "" || new Date(movie.Date).getTime() >= new Date(this.dateFrom).getTime()) &&
       ( this.dateTo == "" || new Date(movie.Date).getTime() <= new Date(this.dateTo).getTime())
      ){ newMovies.push(movie)};

    });
    this.movies = newMovies;
  }
  async delete(idx):Promise<void>{
    fetch('http://localhost:8080/server/delete',{
      method: 'POST',
      body: idx["ID"].toString(),
    }).then(response => {
      if(!response.ok){
        this.service.add({
          severity: "error",
          summary: "Failed!",
          detail: "Failed to delete!"
        }
        );
      }
      else{
          this.service.add({
            severity: "info",
            summary: "Deleted!",
            detail: "Deleted successfully!"
          })
      }
    }
    ).then(() => this.getAllOrders());
  }

  async update(idx):Promise<void>{
    console.log(JSON.stringify(idx));
    fetch('http://localhost:8080/server/update',{
      method: 'POST',
      body: JSON.stringify(idx),
    }).then(response => {
      if(!response.ok){
        this.service.add({
          severity: "error",
          summary: "Failed!",
          detail: "Failed to update!"
        }
        );
      }
      else{
          this.service.add({
            severity: "info",
            summary: "Updated!",
            detail: "Update successful!"
          })
      }
    }
    ).then(() => this.getAllOrders());
  }

  async purchase(idx): Promise<void>{
    fetch('http://localhost:8080/server/buy',{
      method: 'POST',
      body: idx["ID"].toString(),
    }).then(response => {
      if(!response.ok){
        this.service.add({
          severity: "error",
          summary: "Failed!",
          detail: "Failed to purchase!"
        }
        );
      }
      else{
          this.service.add({
            severity: "info",
            summary: "Purchased!",
            detail: "Purchase successful!"
          })
      }
    }
    );
  }

  async getAllOrders(): Promise<void>{
    this.orders = [];
    const data = await fetch('http://localhost:8080/server/getOrders',{
      method: 'GET',
    })
    .then(response => response.json());

    (data as Map<string,object>).forEach((d)=>{
      let m:Order = {ID:0,MovieID:0,Date:'',Net:0,Discount:0,Gross:0};
      m.ID = d["ID"];
      m.Date = d["Date"];
      m.MovieID = d["MovieID"];
      m.Net = d["Net"];
      m.Discount = d["Discount"];
      m.Gross = d["Gross"];
      this.orders.push(m);
    });
  }

  async getAllMovies(): Promise<void>{
    this.movies = [];
    const data = await fetch('http://localhost:8080/server/getMovies',{
      method: 'GET',
    })
    .then(response => response.json());
    
    (data as Map<string,object>).forEach((d)=>{
      let m:Movie = {ID:0,Title:'',Date:'',Price:0,Ranking:0};
      m.ID = d["ID"];
      m.Date = d["date"];
      m.Genre = d["genre"];
      m.Price = d["price"];
      m.Ranking = d["ranking"];
      m.Title = d["name"];
      this.movies.push(m);
    });
  }
}
