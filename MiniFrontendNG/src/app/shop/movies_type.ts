export interface Movie{
    ID: number;
    Title: string;
    Date: string;
    Price: number;
    Ranking: number;
    Genre?: string;
}