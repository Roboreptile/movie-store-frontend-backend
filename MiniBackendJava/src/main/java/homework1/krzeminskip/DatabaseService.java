package homework1.krzeminskip;

import nonapi.io.github.classgraph.json.JSONDeserializer;
import nonapi.io.github.classgraph.json.JSONSerializer;
import org.json.JSONObject;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;


import javax.xml.transform.Result;
import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static java.lang.Math.round;


@Service
public class DatabaseService {
    Connection connection = null;
    List<MovieDescriptor> movies;
    List<OrderDescriptor> orders;

    public DatabaseService() {

        String serverName = "YOGA520";
        String username = "sa";
        String password = "Qwerty11";

        String url = "jdbc:sqlserver://"+serverName+":1433;"
                + "user="+username+";"
                + "password="+password+";"
                + "loginTimeout=10;";
        try {
            connection = DriverManager.getConnection(url);
        } catch(Exception e){
            System.out.println(e.toString());
            e.printStackTrace();
        }

    }
    public void refreshMovies() throws SQLException {
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery("USE Homework1; SELECT * FROM Movies;");
        List<MovieDescriptor> l = new ArrayList<>();
        while(rs.next()){
            MovieDescriptor m = new MovieDescriptor(
                    rs.getInt("MOVIE_ID"),
                    rs.getString("MOVIE_TITLE"),
                    rs.getDate("RELEASE_DATE").toString(),
                    rs.getDouble("PRICE"),
                    rs.getDouble("RANKING"),
                    rs.getString("GENRE")
                    );
            l.add(m);
        }
        this.movies = l;
    }
    public void refreshOrders() throws SQLException{
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery("USE Homework1; SELECT * FROM Orders");
        List<OrderDescriptor> l = new ArrayList<>();
        while(rs.next()){
            OrderDescriptor r = new OrderDescriptor(
                    rs.getInt("ORDER_ID"),
                    rs.getInt("MOVIE_ID"),
                    rs.getDate("ORDER_DATE").toString(),
                    rs.getDouble("NET_AMOUNT"),
                    rs.getDouble("DISCOUNT"),
                    rs.getDouble("GROSS_AMOUNT")
            );
            l.add(r);
        }
        this.orders = l;
    }

    public String getAllMovies(){
        try{
            this.refreshMovies();
        } catch(Exception e){
            return e.toString();
        }
        return JSONSerializer.serializeObject(this.movies);
    }
    public String getAllOrders(){
        try{
            this.refreshOrders();
        } catch(Exception e){
            return e.toString();
        }
        return JSONSerializer.serializeObject(this.orders);
    }


    public String addNewOrder(String idx){
        if(this.movies == null) getAllMovies();
        MovieDescriptor m = null;
        for (MovieDescriptor movie : this.movies) {
            if (movie.getID() == Integer.parseInt(idx)) {
                m = movie;
                break;
            }
        }

        try{
            if(m == null) throw new Exception("Oops");
            System.out.println(m.toString());
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDateTime now = LocalDateTime.now();
            LocalDateTime then = LocalDate.parse(m.getDate(),dtf).atStartOfDay();
            boolean years = Math.abs((int) ChronoUnit.YEARS.between(now.toLocalDate(),then.toLocalDate()))>=5;
            double discount;
            if(years){
                discount = (double) Math.round(m.getPrice()*20)/100;
            } else {
                discount = 0.00;
            }
            Statement stmt = connection.createStatement();
            String query = "use Homework1; "+
                    "set transaction isolation level read committed; "+
                    "begin transaction "+
                    "insert into Orders (MOVIE_ID, ORDER_DATE, NET_AMOUNT, DISCOUNT, GROSS_AMOUNT) values ("+
                     m.getID() +",'"+dtf.format(now)+"',"+ m.getPrice()+"," + discount + "," + (double) Math.round((m.getPrice()-discount)*1.23*100)/100+");"+"commit transaction; SELECT * FROM Orders;";
            ResultSet rs = stmt.executeQuery(query);
            System.out.println(query);
        } catch(Exception e){
            e.printStackTrace();
            return "Oops. Not added.";
        }


        return "Done.";
    }
    public String editOrder(String o){
        try{
            JSONObject map = new JSONObject(o);
            OrderDescriptor r = new OrderDescriptor(map.getInt("ID"),map.getInt("MovieID"),map.getString("Date"),map.getDouble("Net"),map.getDouble("Discount"),map.getDouble("Gross"));
            Statement stmt = connection.createStatement();
            String query = "use Homework1;  "+
                    "set transaction isolation level read committed; "+
                    "begin transaction "+
                    "update Orders "+
                    "set MOVIE_ID = "+r.getMovieID() +
                    ", ORDER_DATE='"+r.getDate() +
                    "', NET_AMOUNT="+r.getNet() +
                    ", DISCOUNT="+r.getDiscount() +
                    ", GROSS_AMOUNT="+r.getGross() +
                    " where ORDER_ID = "+r.getID() +";"+" commit transaction; SELECT * FROM Orders;";
            ResultSet rs = stmt.executeQuery(query);
            System.out.println(query);
        } catch(Exception e){
            e.printStackTrace();
            return "Ooops. Not edited";
        }
        return "Done.";
    }
    public String removeOrder(String idx){
        try{
            Statement stmt = connection.createStatement();
            String query = "use Homework1; "+
                    "set transaction isolation level read committed; "+
                    "begin transaction "+
                    "delete from Orders where ORDER_ID="+idx+";"+" commit transaction; SELECT * FROM Orders;";
            ResultSet rs = stmt.executeQuery(query);
            System.out.println(query);
        } catch(Exception e){
            e.printStackTrace();
            return "Oops. Not removed";
        }
        return "Done.";
    }
}
