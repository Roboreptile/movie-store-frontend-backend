package homework1.krzeminskip;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.sql.Date;

@Data
@AllArgsConstructor
public class OrderDescriptor {
    private int ID;
    private int MovieID;
    private String Date;
    private double Net;
    private double Discount;
    private double Gross;
}
