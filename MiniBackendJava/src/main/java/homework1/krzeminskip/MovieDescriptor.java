package homework1.krzeminskip;


import lombok.AllArgsConstructor;
import lombok.Data;

import java.sql.Date;

@Data
@AllArgsConstructor
public class MovieDescriptor {
    private int ID;
    private String name;
    private String date;
    private double price;
    private double ranking;
    private String genre;
}
