package homework1.krzeminskip;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@Slf4j
@RequestMapping("/server")
public class MockServer {

    @Autowired
    private final  DatabaseService service;
    public MockServer(@Autowired DatabaseService service){
        this.service = service;
    }

    @GetMapping("/getMovies")
    public ResponseEntity<String> getAllM(){
        return ResponseEntity.ok(service.getAllMovies());
    }

    @GetMapping("/getOrders")
    public ResponseEntity<String> getAllO(){
        return ResponseEntity.ok(service.getAllOrders());
    }

    @PostMapping("/buy")
    public ResponseEntity<String> newOrder(@RequestBody final String idx){
        return ResponseEntity.ok(service.addNewOrder(idx));
    }

    @PostMapping("/update")
    public ResponseEntity<String> updateOrder(@RequestBody final String o){
        return ResponseEntity.ok(service.editOrder(o));
    }

    @PostMapping("/delete")
    public ResponseEntity<String> deleteOrder(@RequestBody final String idx){
        return ResponseEntity.ok(service.removeOrder(idx));
    }
}
